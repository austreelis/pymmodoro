"""
Copyright © Morgan Leclerc (2019)

morgan.leclerc@caramail.com

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
"""

__author__ = 'Morgan Leclerc'
__version__ = '1.1 Stable'
__all__ = ['start_pommodoros', 'sounds']


from os.path import exists as _exists
from subprocess import run as _run, PIPE as _PIPE
from time import sleep as _sleep, strftime as _strftime, time as _time,\
    gmtime as _gmtime


sounds = {
    'start': 'pymmodoro/sounds/start.mp3',
    'end': 'pymmodoro/sounds/end.mp3',
    'countdown': 'pymmodoro/sounds/countdown.mp3',
    'finish': 'pymmodoro/sounds/finish.mp3',
}


def _play(sound_file):
    _run(f'mpg123 {sound_file}', stdout=_PIPE, stderr=_PIPE, shell=True)


def _make_pause(length: int, sounds_times: dict, tell_hour) -> dict:
    if 'end' not in sounds_times:
        t = _time()
        _play(sounds['end'])
        sounds_times['end'] = _time() - t
    else:
        _play(sounds['end'])

    if tell_hour is not None:
        if int(tell_hour) == 12:
            command = f'spd-say {_strftime("%I:%M%p")}'
        else:
            command = f'spd-say {_strftime("%H:%M")}'
        _run(command, shell=True, stdin=_PIPE, stderr=_PIPE)

    if 'start' not in sounds_times:
        sounds_times['start'] = 0

    _sleep(max(0, length*60 - sounds_times['end'] - 30 - sounds_times['start']))

    if 'countdown' not in sounds_times:
        t = _time()
        _play(sounds['countdown'])
        sounds_times['countdown'] = _time() - t
    else:
        _play(sounds['countdown'])

    _sleep(max(0, 25 - sounds_times['countdown']))

    for _ in range(int(5/sounds_times['countdown'])):
        _play(sounds['countdown'])

    if sounds_times['start'] == 0:
        t = _time()
        _play(sounds['start'])
        sounds_times['start'] = _time() - t
    else:
        _play(sounds['start'])

    return sounds_times


def _make_pommodoro(length: int, timer: bool, sounds_times: dict) -> dict:
    t = _time()
    if 'start' not in sounds_times:
        _play(sounds['start'])
        sounds_times['start'] = _time() - t
    dt = _time() - t
    timer_msg = ' '
    while dt < length*60:
        if timer:
            timer_msg = _strftime('%H:%M:%S ', _gmtime(length*60 - dt))
        print(timer_msg, end='', flush=True)
        _sleep(1)
        print('\b'*len(timer_msg), end='', flush=True)
        dt = _time() - t
    print(' '*len(timer_msg), end='', flush=True)

    return sounds_times


def _record_pommodoro(count: int,
                      pommodoro_length: int = 25,
                      pause_length: int = 5,
                      timer: bool = True,
                      start_by_pause: bool = False,
                      tell_hour=None,
                      storage='csv'):
    if storage == 'markdown':
        if not _exists('pymmodoro/pymmodoro.md'):
            with open('pymmodoro/pymmodoro.md', 'w') as data_file:
                data_file.writelines(['Completion time | Count | Pommodoro Length '
                                      '(min) | Pause length (min) | Timer ? | '
                                      'Preceded by pause ? | Tell hour ?\n:---:|:--'
                                      '-:|:---:|:---:|:---:|:---:|:---:\n'])
        with open('pymmodoro/pymmodoro.md', 'a') as data_file:
            data_file.writelines([
                f'{_strftime("%Y-%m-%d;%H:%M:%S")} | {count} | {pommodoro_length} |'
                f' {pause_length} | {timer} | {start_by_pause} | {tell_hour}\n'
            ])
    elif storage == 'csv':
        if not _exists('pymmodoro/pymmodoro.csv'):
            with open('pymmodoro/pymmodoro.csv', 'w') as data_file:
                data_file.writelines(['Completion time,Count,Pommodoro Length '
                                      '(min),Pause length (min),Timer ?,'
                                      'Preceded by pause ?,Tell hour ?\n'])
        with open('pymmodoro/pymmodoro.csv', 'a') as data_file:
            data_file.writelines([
                f'{_strftime("%Y-%m-%d;%H:%M:%S")},{count},{pommodoro_length},'
                f'{pause_length},{timer},{start_by_pause},{tell_hour}\n'
            ])
    else:
        raise ValueError(f'Invalid record storage format: {storage}')


def start_pommodoros(count: int,
                     pommodoro_length: int = 25,
                     pause_length: int = 5,
                     starting_count: int = 1,
                     timer: bool = True,
                     start_by_pause: bool = False,
                     tell_hour=None,
                     record='csv'):
    """
    Starts a session of pommodoros for a productive session of work !

    A sound is played when a pommodoro starts, ends, and when the session is
    finished. The path to the files of these sounds is specified in a dict
    pymmodoro.sounds with keys 'start', 'end' and 'finish', respectively.
    Another sound is played a few times by the end of the pause as a countdown,
    which can be found with 'countdown' key. This sounds is played 30 seconds
    before end of pause, then loops for 5 seconds by the end of the pause.

    Start and end sounds can be any lengths, as long as they do not exceed the
    pause time when combined. Countdown sound should be 1 second long or less.
    If it is longer than 5 seconds, the program will not behave as expected.
    Finish sound can be any length.

    :param count: The number of pommodoros to do. -1 to get an endless session.
    :param pommodoro_length: The length in minutes of the pommodoros.
    :param pause_length: The length in minutes of the pause between each
        pommodoro.
    :param starting_count: The starting number of pommodoros already done,
        allowing to restore from an interrupted session.
    :param timer: set to True to display the remaining time of the pommodoro.
    :param start_by_pause: True to start the session with a pause instead of a
        pommodoro. A session always ends with a pommodoro.
    :param tell_hour: 12 to use spd-say to tell you the hour at the end of
        each pommodoro, using 12h format, 24 to use 24h format. Only on linux
         with speech-dispatcher package installed.
    :param record:
    """
    sounds_times = {}

    i = starting_count-1
    while i != count:
        display_count = f'/{count}' if count > 0 else ''
        start_msg = f'\rLet\'s get to work for Pommodoro {i+1}{display_count} !'
        end_msg = f'\rPommodoro {i+1}{display_count} is done, enjoy ' \
            f'{pause_length:2d}m of pause :)'

        # Since len(start_msg) < len(end_msg)
        start_msg += ' '*(len(end_msg) - len(start_msg))

        if start_by_pause:
            sounds_times = _make_pause(pause_length, sounds_times, tell_hour)
        else:
            start_by_pause = True
        print(start_msg, end='')
        sounds_times = _make_pommodoro(pommodoro_length, timer, sounds_times)
        if record != 'no':
            _record_pommodoro(i+1, pommodoro_length, pause_length, timer,
                              start_by_pause, bool(tell_hour), record)
        i += 1
        print(end_msg, end='')
    print()
    _play(sounds['finish'])
