# Pymmodoro

This program is a simple python package / script that allows you to organise
your work sessions using the technics of pommodoros: 25 minutes of work, 5
minutes of pause, then again. This software is released under the [CeCILL-B
license](www.cecill.info)

### Get involved !

__*Do not hesitate to participate to the project. Only Merge Requests adding
<u>one</u> feature, with clean code and clear documentation will be accepted.*__

This may sound a bit hypocrite given the current quality of the code, but just
because the code is not very clean at the moment doesn't mean we can continue
adding piece of code in the same way.

### Disclaimer

The program has been only barely tested, even though I use it on a regular
basis, I can't guarantee it will work straight out of the box for you.

### How to use it

The program needs Python3.6 or higher to run, and mpg123 to play sounds. Make
sure the folder containing the pymmodoro directory is included in your
PYTHONPATH environment variable, or is your current working directory.
Then simply run

`python3 pymmodoro [args]`

See `pymmodoro --help` for more info.

### How the script behave

Depending on the given options, your session may start with a pause.

When the pause starts, the sound `sounds/end.mp3` is played. Then, 30
seconds before the end of the pause, `sounds/countdown.mp3` is played once.
5 seconds before the end of the pause, the same sound is played in loop until
the next pommodoro starts, and the sound `sounds/start.mp3` is played.

If you wish to modify the sounds, there is a dictionary in `__init__.py`
storing the path to the sounds. You may customize them :D

Since the tool used to play sounds notifications blocks, the timings will not be
accurate if `sounds/countdown.mp3` is longer than a second. But we're talking
of about a second or half a second of delay over a work session of several
hours.

### Setting your session's length

You can use two parameters to decide when to end your session: `-u` or `--until`
and `-c` or `--count`.

`--until` takes a time in either the 24h hour format
`HH:MM`, when no "am" not "pm" are specified, or 12h format `HH:MMpp`, where
`pp` is "am" or "pm" (case insensitive). The session will never make it past
given time, but may end sooner (since pomoodoros are of fixed duration).

`--count` is simply the maximum number of pommodoros your session will contain.
You can combine both `--until` and `--count` arguments, or ommit both. When
doing so, hitting <Ctrl-C> ends the program nicely, but your last pommodoro
will not be recorded (if recording is enabled).

### Customizing your session

You can customize several aspects of your session:

* The pommodoros' length with `-L` or `--pommodoro-length` arguments.
* The pauses' length with `-l` or `--pause-length` arguments.
* You can start with a pause using `-p` or `--start-with-pause` arguments.
* You can disable the display of the remaining time using `-T` or `--no-timer`
    arguments.
* You can resume an interrupted session by adding to the last command the `-C`
    or `--starting-count` argument. Since, internally, pymmodoro's only way to
    refer to pommodoros is its"count" (i.e. its index number in the ssession, 
    starting at 1), this will be totally transparent in the record, only
    completion times may help see the session was interrupted.

### Recording your session

By specifying the `-r` parameter, and an optional storage format (for now, 
only supports csv and markdown tables format), you can store every pommodoro
you have done in a file. This is useful if you want to analyse later your
habits, make statistics...

### Other features

Using the `-s` or `--display-stats` arguments, you can display basic statistics
about your pommodors session.

### Known bugs and issues

* The program don't warn when missing `mpg123` to play sounds and will just be
    silent
* Sometimes, the first time a song should be played, it is skipped.
* Stats display only supports markdown table records.
